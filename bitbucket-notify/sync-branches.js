if (process.argv.length >= 3 && process.argv[2] == '-v') {
  console.log(__filename, require('fs').statSync(__filename).mtime);
  process.exit(0);
}

const BitBucket = require('bitbucket');
const bitbucket = new BitBucket();

['BITBUCKET_USERNAME', 'BITBUCKET_PASSWORD', 'SYNC_BRANCHES'].forEach(param => {
  if (!process.env[param]) {
    console.error(`not set env ${param}`);
    process.exit(1);
  }
});

bitbucket.authenticate({
  type: 'apppassword',
  username: `${process.env.BITBUCKET_USERNAME}`,
  password: `${process.env.BITBUCKET_PASSWORD}`
});

const defaultBranch = process.env.PROJECT_DEFAULT_BRANCH || "development";
const newBranch = process.env.CI_COMMIT_REF_NAME;
const projects = process.env.SYNC_BRANCHES.split(',');

if (process.env.CI_COMMIT_REF_NAME == defaultBranch) {
  console.log('its defaultBranch')
  process.exit(0);
}

if (process.env.CI_COMMIT_REF_NAME == 'master') {
  console.log('its master branch')
  process.exit(0);
}

(async () => {
  projects.forEach(async (project) => {
    try {
      const repo = project.split('/');
      const repo_slug = repo[1];
      const username = repo[0];

      const getBranchResult = await bitbucket.refs.getBranch({ name: defaultBranch, repo_slug, username });

      const _body = {
        "name": newBranch,
        "target": {
          "hash": getBranchResult.data.target.hash
        }
      };

      try {
        const createBranchResult = await bitbucket.repositories.createBranch({ _body, repo_slug, username })
        console.log(project, newBranch, 'created');
      }
      catch (err) {
        if (err.error && err.error.data && err.error.data.key && err.error.data.key == 'BRANCH_ALREADY_EXISTS') {
          console.log(project, newBranch, err.error.data.key);
        } else {
          throw err;
        }
      }
    }
    catch (err) {
      if (err.error) {
        console.error(project, err.error);
      } else {
        console.error(project, err);
      }
    }
  });

})()
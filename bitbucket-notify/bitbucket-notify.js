if (process.argv.length >= 3 && process.argv[2] == '-v') {
  console.log(__filename, require('fs').statSync(__filename).mtime);
  process.exit(0);
}

['BITBUCKET_USERNAME', 'BITBUCKET_PASSWORD', 'BITBUCKET_REPO', 'BITBUCKET_USER'].forEach(param => {
  if (!process.env[param]) {
    console.error(`not set env ${param}`);
    process.exit(0);
  }
});

const BitBucket = require('bitbucket')
const bitbucket = new BitBucket()

bitbucket.authenticate({
  type: 'apppassword',
  username: `${process.env.BITBUCKET_USERNAME}`,
  password: `${process.env.BITBUCKET_PASSWORD}`
});

bitbucket.commitstatuses
  .createBuildStatus({
    node: process.env.CI_COMMIT_SHA,
    repo_slug: process.env.BITBUCKET_REPO,
    username: process.env.BITBUCKET_USER,
    _body: {
      key: `${process.env.CI_JOB_NAME}`,
      state: process.argv[2],
      url: `${process.env.CI_PROJECT_URL}/-/jobs/${process.env.CI_JOB_ID}`
    }
  })
  .then(data => console.log('ok'))
  .catch(err => console.error(err));

if (process.env.TELEGRAM_TOKEN) {
  const Telegram = require('telegraf/telegram')
  const telegram = new Telegram(process.env.TELEGRAM_TOKEN);

  const subscribers = [
    process.env.TELEGRAM_SUBSCRIBER
  ]

  subscribers.forEach(subscriber => {
    telegram
      .sendMessage(subscriber, `*${process.argv[2]}*\n${process.env.CI_PROJECT_NAME}\n[${process.env.CI_JOB_NAME}]`, {
        parse_mode: 'Markdown'
      })
      .then(data => console.info(`${subscriber}-ok`))
      .catch(err => console.info(err))
  });
}
const fs = require('fs');
const path = require('path');
const slugify = require('transliteration').slugify;
const execSync = require('child_process').execSync;

if (process.argv.length >= 3 && process.argv[2] == '-v') {
  console.log(__filename, fs.statSync(__filename).mtime);
  process.exit(0);
}

const kubeconfig_file = 'kubeconfig';

const HELM_PROJECT_PREFIX = process.env.CI_PROJECT_NAMESPACE;
const BRANCH_NAME = process.env.CI_COMMIT_REF_NAME;

if (!fs.existsSync(kubeconfig_file)) {
  const KUBERNETES_SERVER = process.env.KUBERNETES_SERVER;
  const KUBERNETES_TOKEN = process.env.KUBERNETES_TOKEN;
  const KUBERNETES_USER = process.env.KUBERNETES_USER;

  if (!KUBERNETES_SERVER || !KUBERNETES_TOKEN || !KUBERNETES_USER) {
    console.log('no kubernetes configs');
    process.exit(1);
  }

  const kubeconfig = `apiVersion: v1
kind: Config
clusters:
- name: "local"
  cluster:
    server: "${KUBERNETES_SERVER}"
    api-version: v1
users:
- name: "${KUBERNETES_USER}"
  user:
    token: "${KUBERNETES_TOKEN}"
contexts:
- name: "local"
  context:
    user: "${KUBERNETES_USER}"
    cluster: "local"
current-context: "local"
`;

  fs.writeFileSync(kubeconfig_file, kubeconfig);
}

const chartPath = path.join(process.cwd(), ".gitlab-ci/chart");

if (!fs.existsSync(chartPath)) {
  console.log('chart not found');
  process.exit(0);
}

const chartName = fs.readdirSync(chartPath)[0];
const chartFullPath = path.join(chartPath, chartName);

const helmNamespace = slugify(BRANCH_NAME);
/* ^(([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9])+$ */
let helmName = `${chartName}-${helmNamespace}`;
if (helmName.length > 53) {
  helmName = helmName.substring(0, 50) + 'mnx'
}

const namespace = `${slugify(HELM_PROJECT_PREFIX)}-${helmNamespace}`;

process.argv.forEach((arg, i) => {
  if (arg == '--createMysqlDatabase') {
    const argsString = (process.argv[i + 1]);
    if (!argsString) {
      console.log('no args is defined');
      process.exit(-1);
    }

    const args = argsString.split(",");
    if (args.length < 3) {
      console.log('need {namespace},{labels},{rootPassword},{prefix}');
      process.exit(-1);
    }

    let databaseName = namespace;

    if (args.length > 3) {
      databaseName += `-${args[3]}`;
    }

    console.log(`Create database ${databaseName}...`);
    const mysqlPod = execSync(`kubectl --kubeconfig=kubeconfig -n ${args[0]} get pod -l${args[1]} -o jsonpath='{.items[0].metadata.name}'`).toString();
    console.log(`find pod... ${mysqlPod}`);
    console.log("Execute create database...");
    execSync(`kubectl --kubeconfig=kubeconfig -n ${args[0]} exec ${mysqlPod} -- mysql -u root -p${args[2]} -e "CREATE DATABASE IF NOT EXISTS \\\`${databaseName}\\\`"`);
    console.log('ok');
  }
})

console.log('Deploy...');

try {
  console.log(execSync(`helm --kubeconfig=${kubeconfig_file} delete --purge ${helmName}`).toString());
} catch (e) {
  /* its not a error */
}

try {
  console.log(execSync(`kubectl --kubeconfig=kubeconfig create namespace ${namespace}`).toString());
} catch (e) {
  /* its not a error */
}

let helm_args = `--set image.tag=${BRANCH_NAME}`

console.log(execSync(`helm --kubeconfig=${kubeconfig_file} install --name ${helmName} --namespace ${namespace} ${helm_args} ${chartFullPath}`).toString());

if (process.env.RANCHER_CLI_CONFIG) {
  console.log('Accept namespace in rancher...');
  const homedir = require('os').homedir();
  const rancherDir = path.join(homedir, '.rancher');
  const rancherDefaultProject = process.env.RANCHER_DEFAULT_PROJECT || 'Default';
  if (!fs.existsSync(rancherDir)) fs.mkdirSync(path.join(homedir, '.rancher'));
  fs.writeFileSync(path.join(rancherDir, "cli2.json"), process.env.RANCHER_CLI_CONFIG);
  console.log(execSync(`rancher namespaces move ${namespace} ${rancherDefaultProject}`).toString());
}

if (process.env.SYNC_BRANCHES) {
  console.log('Sync branches in project group...');
  console.log(execSync('sync-branches').toString());
}